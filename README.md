# Hacked Electric Oven for Reflow Soldering #
* https://embeto.com/reflowster/
* Reflow soldering oven hacked from an electric one.
* One 220VAC power supply is required.
* Internal 220VAC/110VAC transformer required to power the heat extraction fan.
* Internal convection fan.
* Software controlled internal temperature with a custom-made [Nanodroid board](https://embeto.com/nanodroid/), git repo of Nanodroid Board available [here](https://bitbucket.org/embeto/nanodroid/src/master/)
* Internal temperature controlled with Nickel Aluminium Type N thermocouple.
* Oven enclosure isolated with glass wool to ensure the quick rise of temperature required by the solder paste alloy.
* Extraction fan included on the back of the oven to quickly extract the heat from the enclosure in the cooling stage of the reflow process.

## Repository Structure ##
* documents 
	* documentation - information about thermocouple wire soldering
	* schematics - PDF versions of electric schematics 

* pcbDesign 
	* genSchematic
		* only Capture CIS schematic files for the general electrical drawing
	* mainModule (module called DISPLAY in genSchematic)
		* schematics - Working directory for mainModule
		* tapeouts (see *circuit Board design* section for more) - every PCB tapeout is listed here in a directory containing the exact schematic and gerber files
	* powerModule
		* schematics - Working directory for powerModule
		* tapeouts (see *circuit Board design* section for more) - every PCB tapeout is listed here in a directory containing the exact schematic and gerber files

* software
	* arduino - Arduino code used for the nanodroid board which controlled the temperature inside the oven and extraction of heat during the reflow oven 

## Circuit Board design ##

* mainModule (module called DISPLAY in genSchematic)
	* v1.0 - (in-house produced) - This board is used as a hoast for the AD595CQ (AMP THERMOCOUPLE TYPE K) IC, Display, switches and connectors 
	* v1.1 - (in-house produced) - Because in v1.0 connection for the thermocouple wires was omitted, this version was manufactured (not the entire DISPLAY board, just a small extension)

* main_PCB
	* v1.0 - (in-house produced) - Board designed to host the 220VAC/110VAC transformer 


