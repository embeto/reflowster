#include <LiquidCrystal.h>

#define LCD_RS_PIN 12
#define LCD_EN_PIN 11
#define LCD_D4_PIN 5
#define LCD_D5_PIN 4
#define LCD_D6_PIN 3
#define LCD_D7_PIN 2

#define SW1_PIN 6   
#define SW2_PIN 7

#define FAN_PIN           8       //Q1_GATE
#define COOLING_PIN       10      //Q3_GATE gate info depreciated...
#define HEATER_PIN        9       //Q2_GATE gate info depreciated...

#define TEMP_IN_PIN A0
#define POT_IN_PIN A1

#define IDLE_STATE        0x00
#define PREHEAT_STATE     0x01
#define ACTIVATION_STATE  0x02
#define REFLOW_STATE      0x03
#define COOLING_STATE     0x04

#define IDLE_THR        55  //degrees
#define PREHEAT_THR     150 //degrees
#define ACTIVATION_THR  183 //degrees
#define REFLOW_THR      210 //degrees
#define COOLING_THR     60  //degrees

#define NOF_CHECKS_TRANS  2
#define EQUAL_ERROR_EPSILON 0.5 //degrees
#define SLOPE 0.0004 

#define FAN_ON_TIME 20000
#define FAN_OFF_TIME 40000

#define TEMP_ROOM_THRESHOLD 20  
#define NOF_TEMP_SAMPLES 80     //NOF_TEMP_SAMPLES*INTER_SAMPLE_DELAY >= 6800 (analogRead taken anaway 100 us)
#define INTER_SAMPLE_DELAY 40  //in us, delay inter consequtive samples

LiquidCrystal lcd(LCD_RS_PIN, LCD_EN_PIN, LCD_D4_PIN, LCD_D5_PIN, LCD_D6_PIN, LCD_D7_PIN);

int machine_state;
int idle_thr = IDLE_THR;
int curent_nof_checks;
unsigned long int timestamp;
unsigned long int fan_timestamp;
bool fan_state = false;

void setup() {
  pinMode(SW1_PIN, INPUT_PULLUP);
  pinMode(SW2_PIN, INPUT_PULLUP);
  
  pinMode(FAN_PIN, OUTPUT);
  pinMode(COOLING_PIN, OUTPUT);
  pinMode(HEATER_PIN, OUTPUT);
  
  digitalWrite(FAN_PIN, HIGH);          //by default turn on fan
  digitalWrite(COOLING_PIN, LOW);
  digitalWrite(HEATER_PIN, LOW);
  
  lcd.begin(16, 2);

  analogReference(DEFAULT);
    
  machine_state = IDLE_STATE;
  curent_nof_checks = 0;

  fan_timestamp = millis();
}

bool isEqual(float a,float b) {
  if ((a < (b + EQUAL_ERROR_EPSILON)) && (a > (b - EQUAL_ERROR_EPSILON)))
    return true;
  else
    return false;
}

void loop() {

  float temp, avg_temp, nr_samples, exp_temp;

  //THIS TAKES @7000us! (7ms)
  avg_temp = 0;
  nr_samples = 0;
  for(int i=0;i<NOF_TEMP_SAMPLES;i++) {
    temp = analogRead(TEMP_IN_PIN);
    temp = temp*5000/1023;          //this is voltage is mV
    temp = temp/10;                 //this is degrees Celsius
    if (temp > TEMP_ROOM_THRESHOLD) {
      avg_temp += temp;
      nr_samples++; 
    }
    delayMicroseconds(INTER_SAMPLE_DELAY);
  }
  avg_temp /= nr_samples;

  lcd.clear();
  lcd.print("State: ");
  
  switch (machine_state) {
    case IDLE_STATE:
      if ((millis() - fan_timestamp < FAN_ON_TIME) && fan_state == false) {
        digitalWrite(COOLING_PIN, HIGH);
        fan_state = true;
      }
      if ((millis() - fan_timestamp > FAN_ON_TIME) && fan_state == true) {
        digitalWrite(COOLING_PIN, LOW);
        fan_state = false;
      }
      if (millis() - fan_timestamp > FAN_OFF_TIME)
        fan_timestamp = millis();
        
      int pot_val;
      pot_val = analogRead(POT_IN_PIN);
      pot_val = (float)pot_val*90/1023.0;
      idle_thr = (int)pot_val;
      lcd.print("IDLE");
      lcd.setCursor(11,0);
      lcd.print("(");
      lcd.print(idle_thr,DEC);
      lcd.print(")");
      if(idle_thr - avg_temp > EQUAL_ERROR_EPSILON)
        digitalWrite(HEATER_PIN, HIGH);
      if(idle_thr - avg_temp < -EQUAL_ERROR_EPSILON)
        digitalWrite(HEATER_PIN, LOW);
      if (digitalRead(SW1_PIN) == LOW){ //chek also that temp is 60...
        machine_state = PREHEAT_STATE;
        digitalWrite(HEATER_PIN, LOW);
      }
      break;
      
    case PREHEAT_STATE:  
      lcd.print("PREHEAT");
      digitalWrite(HEATER_PIN, HIGH);
      if (avg_temp > PREHEAT_THR) {
        if (curent_nof_checks == NOF_CHECKS_TRANS) {
          curent_nof_checks = 0;
          timestamp = millis();
          machine_state = ACTIVATION_STATE;
          digitalWrite(HEATER_PIN, LOW);
        } else
          curent_nof_checks ++;
      } else 
        curent_nof_checks = 0;
      break;
      
    case ACTIVATION_STATE:  
      lcd.print("ACTIVATE");
      exp_temp = SLOPE * (millis() - timestamp) + PREHEAT_THR;
      
      if (avg_temp > exp_temp) 
        digitalWrite(HEATER_PIN, LOW);
      else
        digitalWrite(HEATER_PIN, HIGH);
      
      if (avg_temp > ACTIVATION_THR) {
        if (curent_nof_checks == NOF_CHECKS_TRANS) {
          curent_nof_checks = 0;
          timestamp = millis();
          machine_state = REFLOW_STATE;
          digitalWrite(HEATER_PIN, LOW);
        } else
          curent_nof_checks ++;
      } else 
        curent_nof_checks = 0;
      break;
    
    case REFLOW_STATE:
      lcd.print("REFLOW");
      digitalWrite(HEATER_PIN, HIGH);
      if (avg_temp > REFLOW_THR) {
        if (curent_nof_checks == NOF_CHECKS_TRANS) {
          curent_nof_checks = 0;
          timestamp = millis();
          machine_state = COOLING_STATE;
          digitalWrite(HEATER_PIN, LOW);
        } else
          curent_nof_checks ++;
      } else 
        curent_nof_checks = 0;
      break; 
    
    case COOLING_STATE: 
      lcd.print("COOLING");
      digitalWrite(COOLING_PIN, HIGH);  
      if (digitalRead(SW2_PIN) == LOW){ //chek also that temp is 60...
        digitalWrite(COOLING_PIN, LOW); 
        machine_state = PREHEAT_STATE;  
      }
      break;
  }

  lcd.setCursor(0, 1);
  lcd.print("Temp: ");
  lcd.print(avg_temp);
  
  delay(350);
  
}
